# Maintainer: Michal Sotolar <michal@sotolar.com>
# Credit goes also to the maintainers and contributors of other PHP versions in
# AUR or official Arch Linux repositories. Specific patches might include code
# from other open source projects. In that case, credit is attributed in the
# specific commit.

pkgbase=php72
_pkgbase=${pkgbase%72}

pkgname=("${pkgbase}"
         "${pkgbase}-cgi"
         "${pkgbase}-apache"
         "${pkgbase}-fpm"
         "${pkgbase}-embed"
         "${pkgbase}-phpdbg"
         "${pkgbase}-dblib"
         "${pkgbase}-enchant"
         "${pkgbase}-gd"
         "${pkgbase}-imap"
         "${pkgbase}-intl"
         "${pkgbase}-odbc"
         "${pkgbase}-pgsql"
         "${pkgbase}-pspell"
         "${pkgbase}-snmp"
         "${pkgbase}-sodium"
         "${pkgbase}-sqlite"
         "${pkgbase}-tidy"
         "${pkgbase}-xsl")

pkgver=
pkgrel=1
pkgdesc="A general-purpose scripting language that is especially suited to web development"
arch=('x86_64')
url='http://www.php.net'
license=('PHP')

makedepends=('apache' 'aspell' 'db' 'enchant' 'gd' 'gmp' 'icu' 'libsodium' 
             'libxslt' 'libzip' 'net-snmp' 'postgresql-libs' 'sqlite' 'systemd'
             'tidy' 'unixodbc' 'curl' 'libtool' 'smtp-forwarder' 'freetds' 'pcre' 
             'c-client' 'libnsl')

checkdepends=('procps-ng')

source=("https://php.net/distributions/${_pkgbase}-${pkgver}.tar.xz"
        "https://php.net/distributions/${_pkgbase}-${pkgver}.tar.xz.asc"
        'apache.patch' 'apache.conf' 'php-fpm.patch' 'php-fpm.tmpfiles'
        'php.ini.patch' 'enchant-2.patch' 'php-freetype-2.9.1.patch' 'icu-70.patch')

sha256sums=(''
            'SKIP'
            '07acff660e194197cfbcc955c0d362d6de063e6475668f3df03bfff023af11ed'
            'da2b1701833700d10f0f01e0c5c665948266fe64b6a2f324fed38a2b6e0c7033'
            '3d70431666000b2371724f409fd1827d5e9d521525f03d99cd93819eaa91ea32'
            '3217979d2ea17f9c6c9209e220d41a5f4e6a6b65fcc7cd5ab8d938f65ca2b59e'
            '1bf35a70d2dce25000e23137a0cf6cde8534c057ddd34b2f28ae1cad8c6a6693'
            'b11c3de747ef222696f5135556d624e3f7f0135a3cb1b06082f1ec8e9b7eeb0a'
            'f9fe57f809ac13e0043d18b795ef777af3e8c710a83745b37b09db536f683d2a'
            '5dbc2d5f3bcfe52d9717d0c9ad42f13f6a56917628afc932c6715e2767b7033f')

validpgpkeys=('B1B44D8F021E4E2D6021E995DC9FF8D3EE5AF27F'
              '1729F83938DA44E27BA0F4D3DBDB397470D12172')

prepare() {
        cd ${srcdir}/${_pkgbase}-${pkgver}

        patch -p0 -i "${srcdir}/apache.patch"
        patch -p0 -i "${srcdir}/php-fpm.patch"
        patch -p0 -i "${srcdir}/php.ini.patch"
        patch -p1 -i "${srcdir}/icu-70.patch"
        patch -p1 -i "${srcdir}/enchant-2.patch"
        patch -p1 -i "${srcdir}/php-freetype-2.9.1.patch"
        autoconf

        rm tests/output/stream_isatty_*.phpt
}

build() {
        # http://site.icu-project.org/download/61#TOC-Migration-Issues
        CPPFLAGS+=' -DU_USING_ICU_NAMESPACE=1'
        CPPFLAGS+=' -DU_DEFINE_FALSE_AND_TRUE=1'

        local _phpconfig="\
                --srcdir=../${_pkgbase}-${pkgver} \
                --config-cache \
                --prefix=/usr \
                --sbindir=/usr/bin \
                --sysconfdir=/etc/${pkgbase} \
                --localstatedir=/var \
                --libdir=/usr/lib/${pkgbase} \
                --datarootdir=/usr/share/${pkgbase} \
                --datadir=/usr/share/${pkgbase} \
                --program-suffix=${pkgbase#php} \
                --with-layout=GNU \
                --with-config-file-path=/etc/${pkgbase} \
                --with-config-file-scan-dir=/etc/${pkgbase}/conf.d \
                --disable-rpath \
                --without-pear \
                "

        local _phpextensions="\
				--enable-bcmath=shared \
				--enable-calendar=shared \
				--enable-dba=shared \
				--enable-exif=shared \
				--enable-ftp=shared \
				--enable-intl=shared \
				--enable-mbstring \
				--enable-shmop=shared \
				--enable-soap=shared \
				--enable-sockets=shared \
				--enable-sysvmsg=shared \
				--enable-sysvsem=shared \
				--enable-sysvshm=shared \
				--enable-zip=shared \
				--with-bz2=shared \
				--with-curl=shared \
				--with-db4=/usr \
				--with-enchant=shared,/usr \
				--with-freetype-dir=/usr \
				--with-gd=shared,/usr \
				--with-gdbm \
				--with-gettext=shared \
				--with-gmp=shared \
				--with-iconv=shared \
				--with-imap-ssl \
				--with-imap=shared \
				--with-kerberos=/usr \
				--with-ldap=shared \
				--with-ldap-sasl \
				--with-libzip \
				--with-mhash \
				--with-mysql-sock=/run/mysqld/mysqld.sock \
				--with-mysqli=shared,mysqlnd \
				--with-openssl \
				--with-password-argon2 \
				--with-pcre-regex=/usr \
				--with-pdo-dblib=shared,/usr \
				--with-pdo-mysql=shared,mysqlnd \
				--with-pdo-odbc=shared,unixODBC,/usr \
				--with-pdo-pgsql=shared \
				--with-pdo-sqlite=shared,/usr \
				--with-pgsql=shared \
				--with-pspell=shared \
				--with-readline \
				--with-snmp=shared \
				--with-sodium=shared \
				--with-sqlite3=shared,/usr \
				--with-tidy=shared \
				--with-unixODBC=shared,/usr \
				--with-xmlrpc=shared \
				--with-xsl=shared \
				--with-zlib \
				--enable-pcntl \
				"

        export EXTENSION_DIR=/usr/lib/${pkgbase}/modules

        # php
        mkdir -p "${srcdir}/build"
        cd "${srcdir}/build"
        ln -sf ../${_pkgbase}-${pkgver}/configure
        ./configure ${_phpconfig} \
                --enable-cgi \
                --enable-fpm \
                --with-fpm-systemd \
                --with-fpm-acl \
                --with-fpm-user=http \
                --with-fpm-group=http \
                --enable-embed=shared \
                ${_phpextensions}
        make

        # apache
        cp -Ta ${srcdir}/build ${srcdir}/build-apache
        cd ${srcdir}/build-apache
        ./configure ${_phpconfig} \
                --with-apxs2 \
                ${_phpextensions}
        make

        # phpdbg
        cp -Ta ${srcdir}/build ${srcdir}/build-phpdbg
        cd ${srcdir}/build-phpdbg
        ./configure ${_phpconfig} \
                --enable-phpdbg \
                ${_phpextensions}
        make
}


check() {
        cd "${srcdir}/${_pkgbase}-${pkgver}"

        # Check if sendmail was configured correctly (FS#47600)
        "${srcdir}"/build/sapi/cli/php -n -r 'echo ini_get("sendmail_path");' | grep -q 'sendmail'

        export REPORT_EXIT_STATUS=1
        export NO_INTERACTION=1
        export SKIP_ONLINE_TESTS=1
        export SKIP_SLOW_TESTS=1

        #"${srcdir}"/build/sapi/cli/php -n run-tests.php -n -P {tests,Zend}
}

package_php72() {
        pkgdesc='A general-purpose scripting language that is especially suited to web development'
        depends=('libxml2' 'curl' 'libzip' 'pcre' 'argon2')
        provides=("${_pkgbase}=$pkgver")
        backup=("etc/${pkgbase}/php.ini")

        cd ${srcdir}/build
        make -j1 INSTALL_ROOT=${pkgdir} install-{modules,cli,build,headers,programs,pharcmd}
        
        install -D -m644 ${srcdir}/${_pkgbase}-${pkgver}/php.ini-production ${pkgdir}/etc/${pkgbase}/php.ini
        install -d -m755 ${pkgdir}/etc/${pkgbase}/conf.d/

        rm -f ${pkgdir}/usr/lib/${pkgbase}/modules/*.a
        rm -f ${pkgdir}/usr/lib/${pkgbase}/modules/{enchant,gd,imap,intl,odbc,pdo_dblib,pdo_odbc,pgsql,pdo_pgsql,pspell,snmp,sodium,sqlite3,pdo_sqlite,tidy,xsl}.so

        rmdir ${pkgdir}/usr/include/php/include
        mv ${pkgdir}/usr/include/php ${pkgdir}/usr/include/${pkgbase}

        rm ${pkgdir}/usr/bin/phar
        ln -sf phar.${pkgbase/php/phar} ${pkgdir}/usr/bin/${pkgbase/php/phar}

        mv ${pkgdir}/usr/bin/phar.{phar,${pkgbase/php/phar}}
        mv ${pkgdir}/usr/share/man/man1/{phar,${pkgbase/php/phar}}.1
        mv ${pkgdir}/usr/share/man/man1/phar.{phar,${pkgbase/php/phar}}.1

        sed -i "/^includedir=/c \includedir=/usr/include/${pkgbase}" ${pkgdir}/usr/bin/${pkgbase/php/phpize}
        sed -i "/^include_dir=/c \include_dir=/usr/include/${pkgbase}" ${pkgdir}/usr/bin/${pkgbase/php/php-config}
        sed -i "/^\[  --with-php-config=/c \[  --with-php-config=PATH  Path to php-config [${pkgbase/php/php-config}]], ${pkgbase/php/php-config}, no)" ${pkgdir}/usr/lib/${pkgbase}/build/phpize.m4
}

package_php72-cgi() {
        pkgdesc='CGI and FCGI SAPI for PHP'
        depends=("${pkgbase}")
        provides=("${_pkgbase}-cgi=$pkgver")

        cd ${srcdir}/build
        make -j1 INSTALL_ROOT=${pkgdir} install-cgi
}

package_php72-apache() {
        pkgdesc='Apache SAPI for PHP'
        depends=("${pkgbase}" 'apache' 'libnsl')
        backup=("etc/httpd/conf/extra/${pkgbase}_module.conf")

        install -D -m755 ${srcdir}/build-apache/libs/libphp7.so ${pkgdir}/usr/lib/httpd/modules/lib${pkgbase}.so
        install -D -m644 ${srcdir}/apache.conf ${pkgdir}/etc/httpd/conf/extra/${pkgbase}_module.conf
}

package_php72-fpm() {
        pkgdesc='FastCGI Process Manager for PHP'
        depends=("${pkgbase}" 'systemd')
        backup=("etc/${pkgbase}/php-fpm.conf" "etc/${pkgbase}/php-fpm.d/php-fpm.conf")
        options=('!emptydirs')

        cd ${srcdir}/build
        make -j1 INSTALL_ROOT=${pkgdir} install-fpm

        install -D -m644 ${srcdir}/build/sapi/fpm/php-fpm.service ${pkgdir}/usr/lib/systemd/system/${pkgbase}-fpm.service
        install -D -m644 ${srcdir}/php-fpm.tmpfiles ${pkgdir}/usr/lib/tmpfiles.d/${pkgbase}-fpm.conf
}

package_php72-embed() {
        pkgdesc='Embedded PHP SAPI library'
        depends=("${pkgbase}" 'libsystemd' 'libnsl')
        provides=("${_pkgbase}-embed=$pkgver")
        options=('!emptydirs')

        cd ${srcdir}/build
        make -j1 INSTALL_ROOT=${pkgdir} PHP_SAPI=embed install-sapi
        mv ${pkgdir}/usr/lib/libphp7.so ${pkgdir}/usr/lib/libphp-72.so
}

package_php72-phpdbg() {
        pkgdesc='Interactive PHP debugger'
        depends=("${pkgbase}")
        provides=("${_pkgbase}-phpdbg=$pkgver")
        options=('!emptydirs')

        cd ${srcdir}/build-phpdbg
        make INSTALL_ROOT=${pkgdir} install-phpdbg
}

package_php72-dblib() {
        pkgdesc='dblib module for PHP'
        depends=("${pkgbase}" 'freetds')
        provides=("${_pkgbase}-dblib=$pkgver")

        install -D -m755 ${srcdir}/build/modules/pdo_dblib.so ${pkgdir}/usr/lib/${pkgbase}/modules/pdo_dblib.so
}

package_php72-enchant() {
        pkgdesc='enchant module for PHP'
        depends=("${pkgbase}" 'enchant')
        provides=("${_pkgbase}-enchant=$pkgver")

        install -D -m755 ${srcdir}/build/modules/enchant.so ${pkgdir}/usr/lib/${pkgbase}/modules/enchant.so
}

package_php72-gd() {
        pkgdesc='gd module for PHP'
        depends=("${pkgbase}" 'gd')
        provides=("${_pkgbase}-gd=$pkgver")

        install -D -m755 ${srcdir}/build/modules/gd.so ${pkgdir}/usr/lib/${pkgbase}/modules/gd.so
}

package_php72-imap() {
        pkgdesc='imap module for PHP'
        depends=("${pkgbase}" 'c-client')
        provides=("${_pkgbase}-imap=$pkgver")

        install -D -m755 ${srcdir}/build/modules/imap.so ${pkgdir}/usr/lib/${pkgbase}/modules/imap.so
}

package_php72-intl() {
        pkgdesc='intl module for PHP'
        depends=("${pkgbase}" 'icu')
        provides=("${_pkgbase}-intl=$pkgver")

        install -D -m755 ${srcdir}/build/modules/intl.so ${pkgdir}/usr/lib/${pkgbase}/modules/intl.so
}

package_php72-odbc() {
        pkgdesc='ODBC modules for PHP'
        depends=("${pkgbase}" 'unixodbc')
        provides=("${_pkgbase}-odbc=$pkgver")

        install -D -m755 ${srcdir}/build/modules/odbc.so ${pkgdir}/usr/lib/${pkgbase}/modules/odbc.so
        install -D -m755 ${srcdir}/build/modules/pdo_odbc.so ${pkgdir}/usr/lib/${pkgbase}/modules/pdo_odbc.so
}

package_php72-pgsql() {
        pkgdesc='PostgreSQL modules for PHP'
        depends=("${pkgbase}" 'postgresql-libs')
        provides=("${_pkgbase}-pgsql=$pkgver")

        install -D -m755 ${srcdir}/build/modules/pgsql.so ${pkgdir}/usr/lib/${pkgbase}/modules/pgsql.so
        install -D -m755 ${srcdir}/build/modules/pdo_pgsql.so ${pkgdir}/usr/lib/${pkgbase}/modules/pdo_pgsql.so
}

package_php72-pspell() {
        pkgdesc='pspell module for PHP'
        depends=("${pkgbase}" 'aspell')
        provides=("${_pkgbase}-pspell=$pkgver")

        install -D -m755 ${srcdir}/build/modules/pspell.so ${pkgdir}/usr/lib/${pkgbase}/modules/pspell.so
}

package_php72-snmp() {
        pkgdesc='snmp module for PHP'
        depends=("${pkgbase}" 'net-snmp')
        provides=("${_pkgbase}-snmp=$pkgver")

        install -D -m755 ${srcdir}/build/modules/snmp.so ${pkgdir}/usr/lib/${pkgbase}/modules/snmp.so
}

package_php72-sodium() {
        pkgdesc='sodium module for PHP'
        depends=("${pkgbase}" 'libsodium')

        install -D -m755 ${srcdir}/build/modules/sodium.so ${pkgdir}/usr/lib/${pkgbase}/modules/sodium.so
}

package_php72-sqlite() {
        pkgdesc='sqlite module for PHP'
        depends=("${pkgbase}" 'sqlite')
        provides=("${_pkgbase}-sqlite=$pkgver")

        install -D -m755 ${srcdir}/build/modules/sqlite3.so ${pkgdir}/usr/lib/${pkgbase}/modules/sqlite3.so
        install -D -m755 ${srcdir}/build/modules/pdo_sqlite.so ${pkgdir}/usr/lib/${pkgbase}/modules/pdo_sqlite.so
}

package_php72-tidy() {
        pkgdesc='tidy module for PHP'
        depends=("${pkgbase}" 'tidyhtml')
        provides=("${_pkgbase}-tidy=$pkgver")

        install -D -m755 ${srcdir}/build/modules/tidy.so ${pkgdir}/usr/lib/${pkgbase}/modules/tidy.so
}

package_php72-xsl() {
        pkgdesc='xsl module for PHP'
        depends=("${pkgbase}" 'libxslt')
        provides=("${_pkgbase}-xsl=$pkgver")

        install -D -m755 ${srcdir}/build/modules/xsl.so ${pkgdir}/usr/lib/${pkgbase}/modules/xsl.so
}
